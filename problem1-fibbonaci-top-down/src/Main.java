import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        System.out.println(fibbonaci((long)5));
        System.out.println(fibbonaci((long)6));
        System.out.println(fibbonaci((long)7));
        System.out.println(fibbonaci((long)40));
        System.out.println(fibbonaci((long)50));
    }

    public static Map<Long, Long> map = new HashMap<>();

    public static Long fibbonaci(Long number) {
        Long fib = (long) 0;
        if (number <= 1) {
            return number;
        } else if (map.containsKey(number)){
            return map.get(number);
        }

        fib = fibbonaci((long) number-1) + fibbonaci((long) number-2);
        map.put((long) number, fib);

        return map.get(number);
    }
}